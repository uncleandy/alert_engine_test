package models

import (
	"context"
	"encoding/json"
	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	TestTechniqueCollectionName = "test_technique"
)

type SourceEvent struct {
	Source		string 		`json:"source"`
	Component	string		`json:"component"`
	Resource	string		`json:"resource"`
	Crit		int			`json:"crit"`
	Message		string		`json:"message"`
	Timestamp	int			`json:"timestamp"`
}

func (src SourceEvent) Update() error {
	dbConn.SetCollection(TestTechniqueCollectionName)

	dbConn.Check()

	filter := bson.D{
		{"component", src.Component},
		{"resource", src.Resource},
		{"status", "ONGOING" },
	}

	changes := bson.D{
		{"crit", src.Crit},
		{"last_msg", src.Message},
		{"last_time", src.Timestamp},
	}

	if src.Crit == 0 {
		changes = append(changes, bson.E{Key: "status", Value: "RESOLVED"})
	}

	update := bson.D{
		{"$set", changes},
	}

	_, err := dbConn.Collection.UpdateOne(context.TODO(), filter, update)

	return err
}

func (src SourceEvent) Insert() error {
	dbConn.SetCollection(TestTechniqueCollectionName)

	dbConn.Check()

	if src.Crit == 0 {
		return nil
	}

	_, err := dbConn.Collection.InsertOne(context.TODO(), bson.D{
		{"component", src.Component},
		{"resource", src.Resource},
		{"crit", src.Crit},
		{"last_msg", src.Message},
		{"first_msg", src.Message},
		{"start_time", src.Timestamp},
		{"last_time", src.Timestamp},
		{"status", "ONGOING"},
	})

	return err
}

func (src *SourceEvent) Parse(event amqp.Delivery) error {
	err := json.Unmarshal(event.Body, src)
	if err != nil {
		return err
	}

	return nil
}
