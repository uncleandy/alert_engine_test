package models

import (
	"github.com/streadway/amqp"
	"testing"
)

func TestParseSourceEvent(t *testing.T) {
	testSet := []string{
		`{ "source": "source 1", "component": "comp 1", "resource": "res 1", "crit": 1, "message": "Текст 1", "timestamp": 1 }`,
		`{ "source": "source 2", "component": "comp 2", "resource": "res 2", "crit": 2, "message": "Текст 2", "timestamp": 2 }`,
		`{ "source": "source 3", "component": "comp 3", "resource": "res 3", "crit": 3, "message": "Текст 3", "timestamp": 3 }`,
	}

	for _, s := range testSet {
		src := &SourceEvent{}
		data := amqp.Delivery{
			Body: []byte(s),
		}
		err := src.Parse(data)
		if err != nil {
			t.Errorf("Parse error: %s", err)
		}
	}
}