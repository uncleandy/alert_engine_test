package models

import "gitlab.com/uncleandy/alert_engine_test/libs/mongodb"

var (
	dbConn	*mongodb.Connection
)

func Init(conn *mongodb.Connection) {
	dbConn = conn
}
