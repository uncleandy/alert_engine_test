package process_events

import (
	"github.com/streadway/amqp"
	"gitlab.com/uncleandy/alert_engine_test/libs/mongodb"
	"gitlab.com/uncleandy/alert_engine_test/libs/rabbitmq"
	"gitlab.com/uncleandy/alert_engine_test/models"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	DefaultTryCount = 100
	DefaultTryDelay = 10 * time.Second
)

func ProcessEvents(rabbit *rabbitmq.Connection, db *mongodb.Connection) {
	// Process signal SIGINT for correct finish
	sigFinish := make(chan os.Signal, 1)
	signal.Notify(sigFinish, syscall.SIGINT)

	// Main loop of process
	for {
		select {
		case event := <-rabbit.Consumer:
			// Read event from rabbitmq
			// Run parse and save event to db
			SaveEventToDB(event)
		case <-sigFinish:
			// Finish process signal
			return
		}
	}
}

func SaveEventToDB(event amqp.Delivery) {
	data := models.SourceEvent{}
	err := (&data).Parse(event)
	if err != nil {
		// Если есть ошибка парсинга JSON - игнорируем такое сообщение
		log.Printf("Error parse JSON: %s\n%s", err, string(event.Body))
		return
	}

	// Цикл для гарантированного сохранения данных
	tryCount := DefaultTryCount
	tryDelay := DefaultTryDelay
	for tryCount > 0 {
		tryCount--

		// Сохраняем данные
		err = data.Update()
		if err == mongo.ErrNoDocuments {
			// Если такой записи еще нет - добавляем ее
			err = data.Insert()
		}

		if err != nil {
			// Если ошибка сохранения данных - выводим об этом сообщение, ждем и пытаемся снова их сохранить
			// Восстановление коннекта есть внутри операций Update и Insert
			log.Printf("Save data error: %s", err)
			time.Sleep(tryDelay)
			continue
		}

		// Если ошибки нет - выходим из цикла
		break
	}
	if err != nil {
		// Если не удалось сохранить данные - падаем с ошибкой
		log.Panicf("Error of save data to mongodb: %s", err)
	}
}
