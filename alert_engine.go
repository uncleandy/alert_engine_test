package main

import (
	"flag"
	"gitlab.com/uncleandy/alert_engine_test/libs/mongodb"
	"gitlab.com/uncleandy/alert_engine_test/libs/rabbitmq"
	"gitlab.com/uncleandy/alert_engine_test/process_events"
	"gitlab.com/uncleandy/alert_engine_test/models"
	"log"
)

const (
	DefaultRabbitUrl 			= "amqp://test:test@localhost:5672/"
	DefaultRabbitChannelName 	= "events"

	DefaultMongoUri 			= "mongodb://localhost:27027"
	DefaultMongoDb 				= "test"
)

var (
	rabbitUrl 			string
	rabbitChannelName   string

	mongoUri 			string
	mongoDb 			string
)

func main() {
	log.Print("Alert engine start...")

	flag.StringVar(&rabbitUrl, "rabbit_url", DefaultRabbitUrl, "Rabbitmq connection URL")
	flag.StringVar(&rabbitChannelName, "rabbit_channel", DefaultRabbitChannelName, "Rabbitmq channel for listen")
	flag.StringVar(&mongoUri, "mongo_uri", DefaultMongoUri, "Mongodb connection URI for save data")
	flag.StringVar(&mongoDb, "mongo_db_name", DefaultMongoDb, "Mongodb database name for save data")
	flag.Parse()

	// Init rabbitmq connection
	rabbit, err := rabbitmq.New(rabbitUrl, rabbitChannelName)
	if err != nil {
		log.Fatalf("Can not connect to RabbitMQ: %s", err)
	}

	// Init mongodb connection
	db, err := mongodb.New(mongoUri)
	if err != nil {
		log.Fatalf("Can not conect to MongoDB: %s", err)
	}
	err = db.Connect()
	if err != nil {
		log.Fatalf("Can not conect to MongoDB: %s", err)
	}
	db.SetDB(mongoDb)

	// Init models
	models.Init(db)

	// Run main loop process
	process_events.ProcessEvents(rabbit, db)

	db.Close()
}
