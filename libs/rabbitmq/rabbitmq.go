package rabbitmq

// Пакет для обслуживания соединения к Rabbitmq серверу

import (
	"github.com/streadway/amqp"
	"log"
)

type Connection struct {
	Addr				string
	ChannelName			string

	Conn    			*amqp.Connection
	Channel 			*amqp.Channel
	Queue   			amqp.Queue
	Consumer			<-chan amqp.Delivery

	EventConnClose 		chan *amqp.Error
	EventChanClose		chan *amqp.Error

	ReconnectMode		bool
	CloseMode			bool
	finish				chan bool
}

func New(addr, channel string) (*Connection, error) {
	connection := Connection{
		Addr: addr,
		ChannelName: channel,
		finish: make(chan bool),
	}

	err := connection.Connect()
	if err != nil {
		return nil, err
	}

	err = connection.OpenChannel()
	if err != nil {
		return nil, err
	}

	// Service goroutines
	go connection.ReconnectMonitor()

	return &connection, nil
}

func (connection *Connection) Close() {
	connection.CloseMode = true
	connection.Conn.Close()
}

func (connection *Connection) Connect() error {
	var err error

	connection.Conn, err = amqp.Dial(connection.Addr);
	if err != nil {
		return err
	}

	oldChan := connection.EventConnClose
	connection.EventConnClose = make(chan *amqp.Error)
	connection.Conn.NotifyClose(connection.EventConnClose)
	if oldChan != nil {
		close(oldChan)
	}

	err = connection.OpenChannel()
	if err != nil {
		_ = connection.Conn.Close()
		return err
	}

	return nil
}

func (connection *Connection) OpenChannel() error {
	var err error

	connection.Channel, err = connection.Conn.Channel()
	if err != nil {
		_ = connection.Conn.Close()
		return err
	}

	connection.Queue, err = connection.Channel.QueueDeclare(
		connection.ChannelName,
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		_ = connection.Channel.Close()
		_ = connection.Conn.Close()
		return err
	}

	connection.Consumer, err = connection.Channel.Consume( connection.Queue.Name,
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		_ = connection.Channel.Close()
		_ = connection.Conn.Close()
		return err
	}

	oldChan := connection.EventChanClose
	connection.EventChanClose = make(chan *amqp.Error)
	connection.Channel.NotifyClose(connection.EventChanClose)
	if oldChan != nil {
		close(oldChan)
	}

	return nil
}

func (connection *Connection) ReconnectMonitor() {
	for {
		select {
		case <-connection.finish:
			log.Print("ReconnectMonitor: Finished")
			return
		case <-connection.EventConnClose:
			if connection.CloseMode {
				continue
			}

			connection.ReconnectMode = true
			log.Print("ReconnectMonitor: Connection reopen...")
			err := connection.Connect()
			if err != nil {
				log.Fatalf("ReconnectMonitor: Cannot connect to rabbitmq: %s", err)
			}
			log.Print("ReconnectMonitor: Connection reopened")
			connection.ReconnectMode = false
		case <-connection.EventChanClose:
			if connection.CloseMode {
				continue
			}

			connection.ReconnectMode = true
			log.Print("ReconnectMonitor: Channel reopen...")
			err := connection.OpenChannel()
			if err != nil {
				log.Fatalf("ReconnectMonitor: Cannot open channel to rabbitmq: %s", err)
			}
			log.Print("ReconnectMonitor: Channel reopened")
			connection.ReconnectMode = false
		}
	}
}
