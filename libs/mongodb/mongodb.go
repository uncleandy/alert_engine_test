package mongodb

// Пакет для обслуживания соединения к Mongodb серверу

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Connection struct {
	Uri				string
	databaseName	string
	collectionName	string

	Client     *mongo.Client
	Database   *mongo.Database
	Collection *mongo.Collection

	ReopenTryCount	int
	ReopenDelay		time.Duration
}

// Создает новое соединение без подключения
func New(uri string) (*Connection, error) {
	connection := Connection{
		Uri: uri,
		ReopenTryCount: 10,
		ReopenDelay: 10 * time.Second,
	}

	return &connection, nil
}

// Производит подключение нового соединения
func (connection *Connection) Connect() error {
	var err error

	connection.Client, err = mongo.NewClient( options.Client().ApplyURI( connection.Uri ) )
	if err != nil {
		return err
	}

	err = connection.Client.Connect(context.Background())
	if err != nil {
		return err
	}

	return nil
}

// Закрывает соединение
func (connection *Connection) Close() {
	_ = connection.Client.Disconnect(context.Background())
}

// Установка текущей БД соединения
func (connection *Connection) SetDB(name string) *Connection {
	connection.databaseName = name
	connection.Database = connection.Client.Database(name)
	return connection
}

// Установка текущей коллекции БД соединения
func (connection *Connection) SetCollection(name string) *Connection {
	connection.collectionName = name
	connection.Collection = connection.Database.Collection(name)
	return connection
}

// Проверка работоспособности соединения
// В случае проблем пытается его переподключить
func (connection *Connection) Check() {
	// Делаем Ping для соединения
	err := connection.Client.Ping(context.Background(), nil)
	if err == nil {
		// Если все хорошо - ничего больше не делаем
		return
	}

	// Try reconnect in loop 10 times with delay 10 seconds
	counter := connection.ReopenTryCount
	for err != nil && counter > 0 {
		time.Sleep( connection.ReopenDelay )
		err = connection.Connect()
		counter--
	}
	if err != nil {
		log.Fatalf("Cannot reconnect to mongodb: %s", err)
	}

	// Если была установлена текущая БД - восстанавливаем ее после переподключения
	if connection.Database != nil {
		connection.SetDB( connection.databaseName )
	}

	if connection.Collection != nil {
		connection.SetCollection( connection.collectionName )
	}
}